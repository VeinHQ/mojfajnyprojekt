echo "Pylint analysis started" && \
DJANGO_SETTINGS_MODULE=forum.settings \
pylint --load-plugins pylint_django --load-plugins pylint_django.checkers.migrations --rcfile=.cicd/config/pylint $@ && \
echo "Bandit analysis started" && \
bandit -c=.cicd/config/bandit.config -r $@ && \
echo "Mypy analysis started" && \
mypy --show-error-codes --config-file .cicd/config/mypy.ini $@
echo "Static analysis finished"

